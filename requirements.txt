Flask
Flask-Cors
nltk
parrot @ git+https://github.com/PrithivirajDamodaran/Parrot_Paraphraser.git@03084c54b64019ba5fa0b620b9c70ad81123e458
torch
transformers
gunicorn
pandas