from flask import Flask, request, jsonify
from flask_cors import CORS
import nltk
import string
from heapq import nlargest
from nltk.corpus import wordnet
import random
from parrot import Parrot
import torch
import warnings
from transformers import BartTokenizer, BartForConditionalGeneration

warnings.filterwarnings("ignore")

# Initialize models and download NLTK data files once
parrot = Parrot(model_tag="prithivida/parrot_paraphraser_on_T5")
tokenizer = BartTokenizer.from_pretrained('facebook/bart-large-cnn')
summarizer = BartForConditionalGeneration.from_pretrained('facebook/bart-large-cnn')

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

app = Flask(__name__)
CORS(app)

def preprocess_text(text):
    stopwords = set(nltk.corpus.stopwords.words('english'))
    nopunc = ''.join(char for char in text if char not in string.punctuation)
    return [word for word in nopunc.split() if word.lower() not in stopwords]

def calculate_word_frequencies(processed_text):
    word_freq = {}
    for word in processed_text:
        word_freq[word] = word_freq.get(word, 0) + 1
    return word_freq

def normalize_word_frequencies(word_freq):
    max_freq = max(word_freq.values())
    return {word: freq / max_freq for word, freq in word_freq.items()}

def score_sentences(text, word_freq):
    sent_list = nltk.sent_tokenize(text)
    sent_score = {}
    for sent in sent_list:
        for word in nltk.word_tokenize(sent.lower()):
            if word in word_freq:
                sent_score[sent] = sent_score.get(sent, 0) + word_freq[word]
    return sent_score

def paraphrase_sentence(sentence):
    paraphrased_sentences = parrot.augment(input_phrase=sentence, use_gpu=False)
    return paraphrased_sentences[0][0] if paraphrased_sentences else sentence

def enforce_punctuation(sentence):
    # Ensure the sentence ends with a dot and nothing else
    if sentence[-1] != '.':
        sentence += '.'
    return sentence

def enforce_capitalization(sentence):
    # Capitalize the first letter of the sentence and ensure nothing else
    sentence = sentence.strip()
    sentence = sentence[0].capitalize() + sentence[1:]
    return sentence

def clean_sentence(sentence):
    # Clean the sentence to ensure correct punctuation and capitalization
    sentence = enforce_capitalization(sentence)
    # Ensure the sentence ends with a dot and nothing else
    sentence = sentence.strip()
    if not sentence.endswith('.'):
        sentence += '.'
    return sentence


def summarize_and_paraphrase(text, num_sentences=5):
    inputs = tokenizer([text], max_length=1024, return_tensors='pt', truncation=True)
    summary_ids = summarizer.generate(inputs['input_ids'], num_beams=4, max_length=50, early_stopping=True)
    summary = tokenizer.decode(summary_ids[0], skip_special_tokens=True)

    sent_score = score_sentences(summary, normalize_word_frequencies(calculate_word_frequencies(preprocess_text(summary))))
    summary_sentences = nlargest(num_sentences, sent_score, key=sent_score.get)

    paraphrased_summary = [paraphrase_sentence(sentence) for sentence in summary_sentences]
    for sentence in paraphrased_summary:
        corrected_summary = [clean_sentence(enforce_punctuation(enforce_capitalization(sentence))) for sentence in paraphrased_summary]
    return corrected_summary

@app.route('/summarize', methods=['POST'])
def summarize():
    data = request.get_json()
    text = data.get('text', '')
    if not text:
        return jsonify({'error': 'No text provided'}), 400

    summarized_text = " ".join(summarize_and_paraphrase(text))
    return jsonify({'summary': summarized_text})

if __name__ == '__main__':
    app.run(debug=True, port=int(os.environ.get('PORT', 5000)), host='0.0.0.0')
