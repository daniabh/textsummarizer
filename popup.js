const textArea = document.getElementById("text_to_summarize");
const submitButton = document.getElementById("submit-button");
const summarizedTextArea = document.getElementById("summary");
const languageSelect = document.getElementById("language-select");

submitButton.disabled = true;

textArea.addEventListener("input", verifyTextLength);
submitButton.addEventListener("click", submitData);
languageSelect.addEventListener("change", changeLanguage);

var selectedLanguage = "en";
function verifyTextLength(e) {
  const textarea = e.target;

  if (textarea.value.length > 200 && textarea.value.length < 100000) {
    submitButton.disabled = false;
  } else {
    submitButton.disabled = true;
  }
}

async function submitData() {
  const textToSummarize = textArea.value;
  const localUrl = "http://localhost:5050"; // Replace with the correct port number
  try {
    const response = await fetch(`${localUrl}/summarize`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ text: textToSummarize })
    });
    console.log(response);
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    summarizedTextArea.value = data.summary;
  } catch (error) {
    console.error("Error summarizing text:", error);
    alert("An error occurred while summarizing the text.");
  }
}


function changeLanguage() {
  selectedLanguage = languageSelect.value;
  updateContent(selectedLanguage);
}

function updateContent(language) {
  const content = {
    en: {
      pageTitle: "Text Summarizer App",
      welcomeText: "Welcome to the AI Text Summarizer App! This app leverages the power of Artificial Intelligence to provide concise summaries of long texts.",
      pasteText: "Simply paste your text into the text area below and click the 'Summarize' button.",
      textAreaPlaceholder: "Paste in some text to summarize. (Min length is 200 chars. Max length is 100,000 chars.)",
      summarizedTextPlaceholder: "Summarized text will appear here",
      summarizeButtonText: "Summarize",
    },
    es: {
      pageTitle: "Aplicación de Resumen de Texto",
      welcomeText: "¡Bienvenido a la aplicación de resumen de texto con IA! Esta aplicación utiliza el poder de la Inteligencia Artificial para proporcionar resúmenes concisos de textos largos.",
      pasteText: "Simplemente pegue su texto en el área de texto a continuación y haga clic en el botón 'Resumir'.",
      textAreaPlaceholder: "Pegue un texto para resumir. (Longitud mínima es 200 caracteres. Longitud máxima es 100,000 caracteres.)",
      summarizedTextPlaceholder: "El texto resumido aparecerá aquí",
      summarizeButtonText: "Resumir",
    },
    fr: {
      pageTitle: "Application de Résumé de Texte",
      welcomeText: "Bienvenue dans l'application de résumé de texte avec IA! Cette application utilise la puissance de l'Intelligence Artificielle pour fournir des résumés concis de textes longs.",
      pasteText: "Collez simplement votre texte dans la zone ci-dessous et cliquez sur le bouton 'Résumer'.",
      textAreaPlaceholder: "Collez du texte à résumer. (Longueur minimale est 200 caractères. Longueur maximale est 100,000 caractères.)",
      summarizedTextPlaceholder: "Le texte résumé apparaîtra ici",
      summarizeButtonText: "Résumer",
    }
  };

  const {
    pageTitle,
    welcomeText,
    pasteText,
    textAreaPlaceholder,
    summarizedTextPlaceholder,
    summarizeButtonText,
  } = content[language];

  document.title = pageTitle;
  document.querySelector(".page-title").textContent = pageTitle;
  document.querySelectorAll("p")[0].textContent = welcomeText;
  document.querySelectorAll("p")[1].textContent = pasteText;
  textArea.placeholder = textAreaPlaceholder;
  summarizedTextArea.placeholder = summarizedTextPlaceholder;
  submitButton.querySelector(".button-text").textContent = summarizeButtonText;
}

const toggleThemeBtn = document.getElementById('toggle-theme-btn');
const themeLink = document.getElementById('theme-link');

toggleThemeBtn.addEventListener('click', function() {
  if (themeLink.getAttribute('href') === 'light-theme.css') {
    themeLink.setAttribute('href', 'dark-theme.css');
  } else {
    themeLink.setAttribute('href', 'light-theme.css');
  }

  const theme = themeLink.getAttribute('href');
  localStorage.setItem('theme', theme);
});

document.addEventListener('DOMContentLoaded', function() {
  const storedTheme = localStorage.getItem('theme');
  if (storedTheme) {
    themeLink.setAttribute('href', storedTheme);
  }
});

// Initialize content based on default language
updateContent(selectedLanguage);
